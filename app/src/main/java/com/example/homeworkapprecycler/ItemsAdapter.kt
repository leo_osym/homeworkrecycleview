package com.example.homeworkapprecycler

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_layout_1.view.*
import kotlinx.android.synthetic.main.item_layout_2.view.*

class ItemsAdapter(val items : ArrayList<Item>, val context: Context): RecyclerView.Adapter<BaseViewHolder>() {

    private val TYPE_1 = 1
    private val TYPE_2 = 2

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(items.get(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        if(viewType == TYPE_1) {
            return ViewHolder1(LayoutInflater.from(context).inflate(R.layout.item_layout_1, parent, false))
        }
        return ViewHolder2(LayoutInflater.from(context).inflate(R.layout.item_layout_2, parent, false))
    }


    override fun getItemViewType(position: Int): Int {
        if(position % 2 ==0){
            return TYPE_1
        }
        return TYPE_2
    }
}

abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(item: Item)
}

class ViewHolder1(view: View): BaseViewHolder(view) {
    override fun bind(item: Item) {
        val text_item_1 = itemView.text_item1
        text_item_1.text = item.content
    }

}
class ViewHolder2(view: View): BaseViewHolder(view) {

    override fun bind(item: Item) {
        val textItem = itemView.text_item2
        val imageItem = itemView.image_item2
        textItem.text = item.content
        imageItem.setImageResource(item.imageId ?: R.drawable.icon_1)
    }

}