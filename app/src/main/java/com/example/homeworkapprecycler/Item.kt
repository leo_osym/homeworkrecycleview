package com.example.homeworkapprecycler

class Item(var layoutContent: String, var layoutImageId: Int?) {

    var content: String
        get() = layoutContent
        set(newValue) {
            layoutContent = newValue
        }

    var imageId: Int?
        get() = layoutImageId
        set(newValue) {
            layoutImageId = newValue
        }

    companion object {
        private var Items: ArrayList<Item> = ArrayList()
        init{
            Items.add(Item("So this is the first entry", null))
            Items.add(Item("So this is the second entry", R.drawable.icon_5))
            Items.add(Item("So this is the third entry", null))
            Items.add(Item("So this is the forth entry", R.drawable.icon_4))
            Items.add(Item("So this is the fifth entry", null))
            Items.add(Item("So this is the sixth entry", R.drawable.icon_2))
            Items.add(Item("So this is the seventh entry", null))
            Items.add(Item("So this is the eighth entry", R.drawable.icon_1))
            Items.add(Item("So this is the ninth entry", null))
            Items.add(Item("So this is the tenth entry", R.drawable.icon_3))
        }
        fun getAllItems(): ArrayList<Item> {
            return Items
        }
    }
}